        %define CALL_SIZE 5
        %define CALL_SIZE_FAR 6
        
        %define BLOCK_DIM 10
        %define HORIZONTAL_SHIFT 1
        %define VERTICAL_SHIFT 1

        %define COLOR_RED 4
        %define ACTIVE_BLOCK_COLOR COLOR_RED

        ;; In 1/18ths of a second
        %define UPDATE_INTERVAL 0x04

        %define SCREEN_HEIGHT 200
        %define SCREEN_WIDTH 320

        ;; Grid is made of BLOCK_DIM sized cells and starts at the
        ;; left edge of the screenn
        %define GRID_HEIGHT ( SCREEN_HEIGHT/BLOCK_DIM - 1 )
        %define GRID_WIDTH ( SCREEN_WIDTH/BLOCK_DIM - 1 )
        %define BYTES_PER_GRID_ROW ( GRID_WIDTH/8 )
        %define GRID_NUM_BYTES ( (GRID_WIDTH) * (GRID_HEIGHT) ) ; one byte per grid block

        %define LEFT_SCANCODE 0x4b
        %define RIGHT_SCANCODE 0x4d

        %define screen_pos(x, y) (SCREEN_WIDTH * (y) + (x))
        %define STARTING_BLOCK_POSITION ( ( GRID_WIDTH / 2 ) )

        ;; BLOCK TYPE DEFINITIONS
        ;; contained in a single word
        ;;   blockPos points to F
        ;; 
        ;; F E D C
        ;; B A 9 8
        ;; 7 6 5 4
        ;; 4 3 2 0

        %define SHAPE_L0 ( (1 << 0xD) | (1 << 9) | (1 << 6) | (1 << 5) )
        %define TEST_SHAPE 0xffff 

        [bits 16]
        [org 0x7c00]

        segment .bss
        
        blockPos resw 1         ; low byte: x, high byte: y
        lastTime resw 1
        timeAccu resw 1
        currShape resw 1
        grid resb GRID_NUM_BYTES

        segment .text
        ;; ---- INIT ----

%if 0        
        ;; DEBUG GRID set to 1 (remove eventually)
        xor ecx, ecx
        mov cx, GRID_NUM_BYTES
clear_grid_loop:
        lea ebx, [grid + ecx]
        mov byte [ebx], 1
        loop clear_grid_loop
%endif        
        
        ;; enter VGA mode
        mov ax, 0x13
        int 0x10

        mov ax, 0xa000
        mov es, ax

        mov word [blockPos], STARTING_BLOCK_POSITION
        mov word [currShape], TEST_SHAPE

;; ---- MAIN LOOP ----
        
main_loop:
        
        ;; ----------------------
        ;; handle input
        ;; check if input waitingn
        mov ax, 0x0100          ; ah = 0x01; preview key
        int 0x16
        jnz handle_input
        jmp input_end

handle_input:
        xor ax, ax
        call set_active_block_grid
        
        xor ax, ax              ; ah = 0x00 bios instruction
        int 0x16
        cmp ah, LEFT_SCANCODE
        je key_left
        cmp ah, RIGHT_SCANCODE
        je key_right
        jmp input_end

key_left:
        ;; constrain the block to the screen (left check)
        mov al, [blockPos]
        cmp al, 1
        jle input_end
        
        sub byte [blockPos], HORIZONTAL_SHIFT
        jmp input_clear_screen

key_right:
        ;; constrain the block to the screen (right check)
        mov al, [blockPos]
        cmp al, GRID_WIDTH - 4
        jge input_end
        
        add byte [blockPos], HORIZONTAL_SHIFT

input_clear_screen:
        call clear_screen

input_end:
        ;; ----------------------
        ;; update active block position based on time
        xor ax, ax
        xor dl, dl
        int 0x1a
        mov ax, dx
        sub ax, [lastTime]
        mov word [lastTime], dx
        add [timeAccu], al
        cmp word [timeAccu], UPDATE_INTERVAL
        jge update_check_bottom
        jmp update_end

update_check_bottom:
        ;; Constrain the block to the bottom of the screen
        mov al, [blockPos + 1]
        cmp al, GRID_HEIGHT - 3
        jge call_new_block
        jmp update

update_check_grid:
        ;; Check here if there is space below the block; if not - new active block

call_new_block:
        call new_active_block
        jmp update_end
        
update: 
        ;; Update block on the grid.
        xor ax, ax
        call set_active_block_grid
        
        mov byte [timeAccu], 0
        add word [blockPos + 1], 1

        call clear_screen

update_end:
        ;; ----------------------

        ;; collision detection
%if 0        
        mov ax, [blockPos]
        mov bx, [blockPos + 1]
%endif
        
        mov byte [grid + 10*GRID_WIDTH + 15], 1
        mov ax, 15
        mov bx, 10
        call get_grid_word
        and ax, [currShape]     
        cmp ax, 0
        jne not_colliding
        call new_active_block
not_colliding:  
        
        ;; update the grid (active block position)
        mov ax, 1        
        call set_active_block_grid

        ;; ----------------------
        ;; draw the grid 
        mov ax, GRID_WIDTH
        mov bx, GRID_HEIGHT
grid_draw_loop:
        mov ecx, GRID_WIDTH
        imul cx, bx
        add cx, ax
        add cx, grid
        mov byte dl, [ecx]
        cmp dl, 1
        jne grid_cell_not_set
        
        push ax
        push bx
        call draw_grid_block
        pop bx
        pop ax
grid_cell_not_set:
        cmp ax, 0
        je grid_row_end
        dec ax
        jmp grid_draw_loop
grid_row_end:
        cmp bx, 0
        je grid_draw_end
	mov ax, GRID_WIDTH
        dec bx
        jmp grid_draw_loop
grid_draw_end:
        jmp main_loop

; ---- SUBROUTINES ----

;; Creates a new active block
new_active_block:
        mov word [blockPos], STARTING_BLOCK_POSITION
        ;; TODO also change the shape of the block to a random one here
        ret

;; Fills in ax with a word containing representation of grid filling at
;; position given by ax (x) and bx (y).
get_grid_word:
        imul bx, GRID_WIDTH
        add ebx, eax
        add ebx, grid            ; ebx contains the interesting grid part address now; ax is our return value
        mov byte [ebx], 1
        mov cl, 15              ; iterate over each grid cell in a 4x4 square at [eax]
        xor ax, ax
get_grid_word_loop:
        ;; 16 needs to fill the most significat bit of the output word, so what
        ;; we want is a right shift by cl bits.
        lea edx, [ebx + ecx]
        push ebx        
        xor bx, bx              ; may be unnecessary
        mov bl, [edx]
        shr bx, cl
        or ax, bx
        pop ebx
        cmp cl, 0
        je get_grid_word_return ; TODO I think there is an appropriate loop instruction that could be used here.
        dec cl
        jmp get_grid_word_loop
get_grid_word_return:
        ret
        
;; Sets value of grid cells corresponding to the active block to
;; the one contained in ax
set_active_block_grid:  
        ;; iterate through every bit in shape definition
        mov dl, al
        mov cx, 15
block_update_loop:
        mov ax, [currShape]
        shr ax, cl              ; 01001010 for cx = 4 will be 0
        and ax, 1
        cmp ax, 1
        jne block_update_wrap
pos_is_set:
        mov ax, cx
        mov bl, 4
        idiv bl
        ;; al contains y offset, ah contains x offset now
        xor bx, bx        
        mov bl, [blockPos + 1]
        add bl, al
        mov al, ah
        xor ah, ah
        add al, [blockPos]
        imul bx, GRID_WIDTH
        add ax, bx
        lea ebx, [eax + grid]
        mov byte [ebx], dl
        jmp block_update_wrap
block_update_wrap:
        cmp cx, 0
        je block_update_end
        dec cx
        jmp block_update_loop
block_update_end:
        ret
        
;; draws a row of grid
;; ax grid x,
;; bx grid y,
draw_grid_block:
        imul ax, BLOCK_DIM
        imul bx, BLOCK_DIM * SCREEN_WIDTH
        add ax, bx
        call draw_block
        ret

;; clears the screen (black color)
clear_screen:
        ;; clear screen
        xor ax, ax
        xor di, di
        cld
        mov cx, 32000
        rep stosw
        ret
        
;; draws a block on the screen.
;; in:  ax, position of upper left corner of the block
;;      dx, return address
;; out: ax, position of upper right corner of the block
;; invalidates: bx, cx, di
draw_block:     
        mov cx, BLOCK_DIM
draw_block_loop:
        ;; top line pixel
        mov di, ax
        mov byte [es:di], ACTIVE_BLOCK_COLOR

        ;; left line pixel
        mov bx, SCREEN_WIDTH
        imul bx, cx
        add di, bx
        mov bx, BLOCK_DIM
        sub bx, cx
        sub di, bx
        mov byte [es:di], ACTIVE_BLOCK_COLOR

        ;; right line pixel
        add di, BLOCK_DIM
        mov byte [es:di], ACTIVE_BLOCK_COLOR

        ;; bottom line pixel
        mov di, ax
        add di, SCREEN_WIDTH * BLOCK_DIM
        mov byte [es:di], ACTIVE_BLOCK_COLOR
        inc ax
        loop draw_block_loop
        
        ;; draw upper-right corner pixel
        mov byte [es:eax], ACTIVE_BLOCK_COLOR
        ret


        times 510 - ($ - $$) db 0
        dw 0xaa55
